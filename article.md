---
title: "Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu"
perex: "Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi."
description: "Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi."
authors: ["Jan Boček", "Jan Cibulka"]
published: "20. října 2016"
coverimg: https://interaktivni.rozhlas.cz/umrti-srdce/media/cover.jpg
socialimg: https://interaktivni.rozhlas.cz/umrti-srdce/media/socimg.png
coverimg_note: "Foto <a href='https://www.flickr.com/photos/sgtfun/60558327/'>sgt fun</a> (CC BY-NC-ND 2.0)"
url: "umrti-srdce"
libraries: [topojson, leaflet, jquery]
recommended:
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/jizni-mesto-sidliste-bez-lidi--1532519
    title: Jižní Město – sídliště bez lidí?
    perex: Skoro deset tisíc lidí zmizelo z největšího tuzemského sídliště od roku 2001. Kam a proč? To je otázka důležitá pro všechny obyvatele paneláků. Podle posledního sčítání jich je v Česku 2,76 milionu. Mají ze svých sídlišť radši také rychle zmizet?
    image: http://media.rozhlas.cz/_obrazek/3467578.jpeg
  - link: https://interaktivni.rozhlas.cz/sudety/
    title: Existují Sudety? Hranice jsou zřetelné i sedmdesát let po vysídlení Němců
    perex: Statistiky nezaměstnanosti, kvality vzdělání nebo volební účasti v Česku dodnes kopírují hranice historických Sudet. Vlnu osadníků připomíná také nejsilnější příhraniční menšina – Slováci. Prohlédněte si detailní mapy.
    image: https://samizdat.cz/data/sudety-clanek/www/media/slovaci.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/rusove-proti-rusum-zeme-v-nevyhlasene-obcanske-valce--1484099
    title: Rusové proti Rusům: Země v nevyhlášené občanské válce
    perex: Rusko prošlo v devadesátých letech demografickou krizí srovnatelnou s válečným konfliktem. Za Putinovy vlády se pád do propasti zpomalil, snad i zastavil. To ale nemusí vydržet dlouho: nynější růst stojí na velmi křehkých základech.
    image: http://media.rozhlas.cz/_obrazek/3372356.jpeg
---

Ve východním Polsku a na západě Rumunska jsou nejnebezpečnější řidiči v Evropě. V Estonsku je největší riziko otravy – typicky alkoholem – a virus HIV zabíjí téměř výlučně v jižní Itálii.

To vše prozrazují data Eurostatu za roky 2011 až 2013. Čísla na mapě ukazují takzvanou standardizovanou mortalitu: roční počet úmrtí na 100 000 obyvatel daného území, přepočtený na stejnou věkovou strukturu. Tím se eliminuje vliv různého průměrného věku na jednotlivých územích a čísla lze proto navzájem srovnávat. Mortalita přímo souvisí s očekávanou délkou dožití – tam, kde je nižší, se žije déle.

<aside class="big">
  <div class="selector" id="selector" onchange="checkSelect()">
    <select id="diagnoza" name="diagnoza">
      <option value="diagnoza1">Celková úmrtnost</option>
      <option value="diagnoza3">Nemoci oběhové soustavy</option>
      <option value="diagnoza4">Nemoci dýchací soustavy</option>
      <option value="diagnoza8">Rakovina prsu</option>
      <option value="diagnoza9">Rakovina tlustého střeva</option>
      <option value="diagnoza10">Rakovina prostaty</option>
      <option value="diagnoza11">Rakovina plic</option>
      <option value="diagnoza14">Cukrovka</option>
      <option value="diagnoza6">HIV</option>
      <option value="diagnoza5">Vnější příčiny</option>
      <option value="diagnoza7">Sebevražda</option>
      <option value="diagnoza12">Dopravní nehody</option>
      <option value="diagnoza2">Napadení</option>
      <option value="diagnoza13">Otrava (včetně alkoholu)</option>
      <option value="diagnoza15">Pády</option>
    </select>
  </div>
  <div id="mapid"></div>
</aside>

Data o mortalitě ukazují, že mezi západní a východní polovinou Evropy zeje propast. Na jednom pólu statistiky leží Madrid se 763 úmrtími na 100 tisíc obyvatel, na opačném pólu je severozápad Bulharska s 1810 mrtvými na 100 tisíc lidí ročně.

Česko v mortalitě, a tedy i očekávané délce dožití, hraje roli mostu mezi západem a východem. Výraznou výjimkou je pouze Ústecký a Karlovarský kraj, kde je úmrtnost výrazně vyšší, a Praha, kde je naopak podstatně nižší. Praha tím kopíruje trend, který je zvlášť na východě Evropy poměrně výrazný: obyvatelé hlavních měst žijí déle.

Výjimkou z rozdělení západ-východ je pouze Slovinsko. Podle dat Eurostatu je zde vůbec nejnižší mortalita z celé Evropy. Slovinci se také průměrně dožívají stejného věku jako Němci nebo Britové.

## Češi umí nemoci srdce léčit, ale zapomněli na prevenci

Nejčastější příčinou úmrtí jsou ve většině evropských zemí nemoci oběhové soustavy – infarkt, nemoci a vady srdce nebo mozková mrtvice. Právě tahle kategorie je proto kritická pro snižování celkové úmrtnosti. A právě tady jsou rozdíly mezi starými a novými členy unie největší: v Paříži na nemoci srdce ročně umírá 174 lidí na každých 100 tisíc obyvatel, na severozápadě Bulharska je to 1250 lidí.

„Je prokázaný pozitivní vliv vysokého životního standardu na výskyt ischemické choroby srdce,“ řekl Českému rozhlasu profesor Jan Marek z Institutu kardiovaskulárních chorob na londýnské University College. „Velké rozdíly jsou i lokální: pravděpodobnost této choroby ve východním Londýně je sedmkrát větší než v západním Londýně. Aktivní život, dobrá životospráva a prevence jsou zcela jistě určující faktory. Jen to stojí peníze a ty v Bulharsku moc nejsou. To, že jí daleko víc ryb než my Češi, nestačí.“

Vliv stravovacích návyků ilustruje také nižší mortalita v chudších evropských zemích, jako je Portugalsko nebo Řecko.

„Středomořské národy mají výhodu zdravého stravování a nemusí se zas tak moc hýbat,“ pokračuje Marek. „Ve středomoří hraje roli také genetika, stejně jako třeba v Japonsku. Tam ale zase mají nejvyšší výskyt rakoviny tlustého střeva; čili jíst syrové ryby asi není úplně ideální.“

Suverénně nejnižší mortalitu na nemoci srdce má Francie. Vědci zatím neodhalili, v čem se tajemství Francouzů skrývá: rádi si dopřávají tučnou stravu i hovězí maso, což by mělo výskyt oběhových nemocí zvyšovat. Ve skutečnosti se tak neděje.

Česko je v léčbě oběhových nemocí na světové špičce. Počet nemocných je ale tak vysoký, že i přes skvělou lékařskou péči patříme k evropským zemím, kde na oběhové nemoci umírá nejvíc lidí. „Šance na přežití akutního infarktu myokardu je v Česku nejvyšší na světě,“ popisuje pokrok v posledním čtvrtstoletí profesor Jan Marek. „Zároveň ale Češi zapomněli na prevenci, všechno řeší jenom lékařskými zákroky. Proto je úmrtnost na ischemickou chorobu srdce v Praze, kde jsou špičková pracoviště, nižší než třeba v Ostravě,“ dodává Marek.

## Když nevíte, napište infarkt

Data o mortalitě ovšem mají rezervy. Lékaři napříč Evropou sice mají stejný cíl – dopátrat se původní příčiny úmrtí – ale zejména u méně početných diagnóz se můžou projevit místní zvyklosti. Lékaři si pomáhají například tam, kde není příčina úmrtí jistá.

„Údaje o úmrtnosti jsou poměrně tvrdá data, zatímco údaje o příčinách smrti měkká, tedy velmi nespolehlivá,“ vysvětluje Jan Holčík, bývalý vedoucí Ústavu sociálního lékařství a veřejného zdravotnictví na Lékařské fakultě Masarykovy univerzity. „Jeden čas byl v Česku jako příčina smrti neobvykle vysoký výskyt srdečních infarktů. Data nepůsobila věrohodně a Eurostat nakonec došel k závěru, že když čeští lékaři neznají příčinu smrti, tak napíší, že jde o infarkt.“

„Příčina smrti je velmi podmíněna diagnostikou, dostupností preventivních prohlídek, celou řadou dalších faktorů a zejména propitvaností,“ dodává Holčík. „Když se pitvá 10 až 20 procent zemřelých – v některých oblastech i méně – pak jsou údaje o příčinách smrti jen odhady, závisející na kultuře a solidnosti ošetřujících lékařů, která se ovšem mezi východem a západem může lišit.“